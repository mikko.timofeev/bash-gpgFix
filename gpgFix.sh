#!/bin/bash
if [ -z "$1" ]; then
	read -p "Enter GPG key ID: " keyId;
	if [ -z "$keyId" ]; then
	        echo "Operation aborted";
	fi
else
	keyId=$1;
fi
gpg --recv-keys $keyId;
sudo pacman-key -r $keyId;
sudo pacman-key --lsign-key $keyId;
sudo pacman -Su;
if [ -z "$1" ]; then
	read -p "Press Enter key to exit";
fi
